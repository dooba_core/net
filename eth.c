/* Dooba SDK
 * Generic Network Stack
 */

// External Includes
#include <stdlib.h>
#include <util/cprintf.h>

// Internal Includes
#include "eth.h"

// Int to String
void net_eth_itoa(uint8_t *addr, char *buf)
{
	uint16_t l;
	l = csnprintf(buf, NET_ETH_ADDR_LEN_TXT, "%h%h:%h%h:%h%h:%h%h:%h%h:%h%h", (uint16_t)((addr[0] >> 4) & 0x0f), (uint16_t)(addr[0] & 0x0f), (uint16_t)((addr[1] >> 4) & 0x0f), (uint16_t)(addr[1] & 0x0f), (uint16_t)((addr[2] >> 4) & 0x0f), (uint16_t)(addr[2] & 0x0f), (uint16_t)((addr[3] >> 4) & 0x0f), (uint16_t)(addr[3] & 0x0f), (uint16_t)((addr[4] >> 4) & 0x0f), (uint16_t)(addr[4] & 0x0f), (uint16_t)((addr[5] >> 4) & 0x0f), (uint16_t)(addr[5] & 0x0f));
	buf[l] = 0;
}

// String to Int (buf must be at least NET_ETH_ADDR_LEN)
void net_eth_atoi(char *addr, uint8_t *buf)
{
	uint8_t i;
	char x[5];
	x[0] = '0';
	x[1] = 'x';
	for(i = 0; i < NET_ETH_ADDR_LEN; i = i + 1)
	{
		x[2] = addr[(i * 3) + 0];
		x[3] = addr[(i * 3) + 1];
		x[4] = 0;
		buf[i] = strtoul(x, 0, 0);
	}
}
