/* Dooba SDK
 * Generic Network Stack
 */

#ifndef	__NET_IP_H
#define	__NET_IP_H

// External Includes
#include <stdint.h>

// IP Address Length
#define	NET_IP_ADDR_LEN										4
#define	NET_IP_ADDR_LEN_TXT									16

// Int to String (buf must be at least NET_IP_ADDR_LEN_TXT)
extern void net_ip_itoa(uint32_t addr, char *buf);

#endif
