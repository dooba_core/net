/* Dooba SDK
 * Generic Network Stack
 */

#ifndef	__NET_ETH_H
#define	__NET_ETH_H

// External Includes
#include <string.h>
#include <stdint.h>

// Ethernet Address Length
#define	NET_ETH_ADDR_LEN										6
#define	NET_ETH_ADDR_LEN_TXT									18

// NULL Address
#define	NET_ETH_ADDR_NULL											((uint8_t *)"\x00\x00\x00\x00\x00\x00")

// Broadcast Address
#define	NET_ETH_ADDR_BCAST											((uint8_t *)"\xff\xff\xff\xff\xff\xff")

// Printf Arguments for Ethernet Address
#define	NET_ETH_PRINTF_ARGS(a)										a[0], a[1], a[2], a[3], a[4], a[5]

// Is Address NULL?
#define	NET_ETH_ADDR_IS_NULL(a)										(memcmp(a, NET_ETH_ADDR_NULL, NET_ETH_ADDR_LEN) == 0)

// Ethernet Header Structure
struct net_eth_head
{
	// Destination
	uint8_t dst[NET_ETH_ADDR_LEN];

	// Source
	uint8_t src[NET_ETH_ADDR_LEN];

	// EtherType
	uint16_t etype;
};

// Int to String (buf must be at least NET_ETH_ADDR_LEN_TXT)
extern void net_eth_itoa(uint8_t *addr, char *buf);

// String to Int (buf must be at least NET_ETH_ADDR_LEN)
extern void net_eth_atoi(char *addr, uint8_t *buf);

#endif
