/* Dooba SDK
 * Generic Network Stack
 */

// External Includes
#include <util/cprintf.h>

// Internal Includes
#include "ip.h"

// Int to String
void net_ip_itoa(uint32_t addr, char *buf)
{
	uint16_t l;
	l = csnprintf(buf, NET_IP_ADDR_LEN_TXT, "%i.%i.%i.%i", (uint16_t)((addr >> 24) & 0x000000ff), (uint16_t)((addr >> 16) & 0x000000ff), (uint16_t)((addr >> 8) & 0x000000ff), (uint16_t)(addr & 0x000000ff));
	buf[l] = 0;
}
