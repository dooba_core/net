/* Dooba SDK
 * Generic Network Stack
 */

#ifndef	__NET_WIFI_H
#define	__NET_WIFI_H

// External Includes
#include <stdint.h>

// Acess Point Name Length
#define	NET_WIFI_AP_NAME_LEN										32

#endif
